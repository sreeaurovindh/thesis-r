Thesis Code. 
========================================================

This document describes various transformation that are performed as a part of my Masters Thesis.

## Basic Steps

* Extraction of data and Processing of data
* Segmentation of overall Episodes
* Extraction of various features
* Machine Learning
* Output, ROC Curves and Interpretation.

### 1. Extraction and Processing of data

#### Library and File name to be extracted

```r
library(XML)
library(plyr)
library(tuneR)
```

```
## tuneR >= 1.0 has changed its Wave class definition.
## Use updateWave(object) to convert Wave objects saved with previous versions of tuneR.
```

```r
file.name <- 'elanFiles\\tagged.eaf'
```


#### Method to extract a tier from an elan file


```r
GetActionTier <- function(file.name,tag.name,eventActions){
  # This method creates a table of annotation Ids, timestamps along with their annotation descriptoins
  #
  # Args : 
  #   fileName : Name of the Elan annotated File
  #   eventActions :  Boolean to denote if student actions are extracted
  xml.file <- xmlTreeParse(file.name)
  top <- xmlRoot(xml.file)
  collabNodeSet<- getNodeSet(top,paste("//TIER[@TIER_ID='",tag.name,"']/ANNOTATION/ALIGNABLE_ANNOTATION",sep=""))
  if(is.null(collabNodeSet)){
    print("Tier name is Wrong")
    return("Tier name is Wrong")
  }
  ref1 <- sapply(collabNodeSet,function(x) xmlGetAttr(x,"TIME_SLOT_REF1"))
  ref2 <- sapply(collabNodeSet,function(x) xmlGetAttr(x,"TIME_SLOT_REF2"))
  
 
  
  # Get all ANNOTATION Values for Student tier
  annotationsSegment<- getNodeSet(top,paste("//TIER[@TIER_ID='",tag.name,"']/ANNOTATION/ALIGNABLE_ANNOTATION/ANNOTATION_VALUE",sep=""))
  annotations <- sapply(annotationsSegment,function(x) xmlValue(x))
  # There are some empty outputs if there is no annotations present. So We should convert them into NA's to prevent 
  # Problems.
  annotations[sapply(annotations,function(x) length(x) == 0)] <- NA
  annotationElements <- unlist(annotations)
  if(eventActions){
  allAnnotationElements<- sapply(annotationElements,function(x) strsplit(x,"\\|")[[1]])
  eventData <- data.frame()
  for(i in seq(along=allAnnotationElements)){
    for(j in 1:9){
      eventData[i,j] <- allAnnotationElements[[i]][j]
    }
  }
}
  outTable <- cbind(ref1,ref2)
  outTimeSlots <- deriveTimeSlots(top)
  outTimeGrid<- getActualTime(outTable,outTimeSlots)
  timedf<-as.data.frame(outTimeGrid)
  colnames(timedf)<- c("start","end")
  if(eventActions){
    eventData <- cbind(eventData,timedf)
  }else{
    eventData <- cbind(annotationElements,timedf)
 }
  eventData
}
```

#### Extract Time slots

This is a helper method that is used to extract time slots from the Elan Tier. 


```r
deriveTimeSlots <- function(top){
  timeSlotSet <- getNodeSet(top,"//TIME_ORDER/TIME_SLOT[@TIME_SLOT_ID]")
  timeSlot_ids <- sapply(timeSlotSet,function(x) xmlGetAttr(x,"TIME_SLOT_ID"))
  timeSlot_vals <- sapply(timeSlotSet,function(x) xmlGetAttr(x,"TIME_VALUE"))
  outTimeSlots <- cbind(timeSlot_ids,timeSlot_vals)
  outTimeSlots
}

getActualTime<-function(outTable,outTime){
  table<- as.data.frame(outTable,stringsAsFactors = FALSE)
  time<- as.data.frame(outTime,stringsAsFactors = FALSE)
  colnames(table)<-c("ref1","ref2")
  colnames(time)<- c("ref","time")
  ref1Times<- sapply(table$ref1,function(x) time[time$ref==x,2])
  ref2Times<- sapply(table$ref2,function(x) time[time$ref==x,2])
  timeGrid <- cbind(as.double(ref1Times),as.double(ref2Times))
  timeGrid
}
```




#### Remove Interruptions
This method removes interupptions from the Study data points

```r
removeInterruptions <- function(dataset,interruption){
  # This function is used to remove all interruptions from the data tier.
  #
  # Args:
  # dataset : The dataset from which datapoints has to be removed
  # Interruptions : The interruptions that has to be removed.
  
  # Creates an accept matrix for each of the interruption vector
  accept.matrix<- apply(interruption,1,function(x) removePoint(dataset,x))
  #  Performs a logical and reduces it for every row.
  accept<-apply(accept.matrix,1,function(x) Reduce('&',x))
  dataset[accept,]
}
```

### Accept and Remove Points

This method accepts all the set of points that are inside of the duration tier and remove the ones 
that are in the interruption tier

```r
acceptPoints <-function(data,duration.point){
  ac1 <- as.numeric(duration.point$start)
  ac2 <- as.numeric(duration.point$end)
  
  startBetween <- ( data$start >= ac1  &  data$start<= ac2)
  endBetween <- ( data$end >= ac1  &  data$end<= ac2)
  choose <- (startBetween & endBetween)
  data[choose,]
}

removePoint<- function(data, inter.point){
  # chooses all the relevent points and removes the ones that aren't
    ex1 <- as.numeric(inter.point["start"])
    ex2 <- as.numeric(inter.point["end"])
    
    startBetween <- ( data$start > ex1  &  data$start< ex2)
    endBetween <- ( data$end > ex1  &  data$end< ex2)
    equal <- (ex1 == data$start & ex2 == data$end)
    
    choose <- ! (startBetween | endBetween | equal)
    choose
}
```


### Assign Row and Column as it is given as a grid
This method assigns a row and column to each card if it is inside the solution grid.


```r
assignCurrentRow <- function(move.point,ans){
  #This funciton will assign row and column number to each card
   cardX <- as.numeric(move.point["cardX"])
   cardY <- as.numeric(move.point["cardY"])
   
   
   card.inside.boolean<- (ans$LeftX <= cardX & ans$RightX > cardX & ans$TopY <= cardY & ans$BotY > cardY)
   soln.grid.matches<- ans[card.inside.boolean,]
   if(nrow(soln.grid.matches) >= 1){
      position<- soln.grid.matches[1,c("Row","Col")]
   }else{
     position <- data.frame()
     position <- rbind(position,-1)
     position <- cbind(position,-1)
     names(position)<- c("Row","Col")
     
   }
   position
   
   
}
```

### Omit interruptions
This function recalulates the start and end time after the interuptions are removed


```r
 adjustInterval <- function(interval.point,student){
  endtime <- interval.point$end
  interval <- interval.point$interval
  student[student$start > endtime,c("adj.start","adj.end")] <- student[student$start > endtime,c("adj.start","adj.end")] - interval
  student
}
```



### Read All data Points and Time intervals

This function does the following
* Reads the data
* Calculates the raw Duration
* Processes  the interruptions and calculates the adjusted durations


```r
  duration <- GetActionTier(file.name,'duration',FALSE)
  interuption <- GetActionTier(file.name,'interruption',FALSE)
  student1 <- GetActionTier(file.name,'student1',TRUE)
  student2 <- GetActionTier(file.name,'student2',TRUE)
  #Select Valid student Points
  student1<- acceptPoints(student1,duration)
  student2 <- acceptPoints(student2,duration)
  student1 <- removeInterruptions(student1,interuption)
  student2 <- removeInterruptions(student2,interuption)
  
  # Calculate adjusted start and end time
  interuption$interval <- interuption$end - interuption$start
  # Duplicate the start and end time rows for calculations for each student
  student1$adj.start <- student1$start
  student1$adj.end <- student1$end
  student2$adj.start <- student2$start
  student2$adj.end <- student2$end
  

  # Since removal of interuption tiers have already been done 
  # There shouldn't be any overlaps.
  # Remove the interruption time from the actual tiers.
  for(i in 1:nrow(interuption)){
    student1 <- adjustInterval(interval.point=interuption[i,],student=student1)
  }
  
  for(j in 1:nrow(interuption)){
    student2 <- adjustInterval(interval.point=interuption[j,],student=student2)
  }
```

### Extract Card based events from the log file

```r
card.move.s1 <- student1[student1$V1=='CardMoveCommand',]
card.move.s2 <- student2[student2$V1=='CardMoveCommand',]
card.move.s1  <- card.move.s1[!is.na(card.move.s1$V1),]
card.move.s2  <- card.move.s2[!is.na(card.move.s2$V1),]

# Assign Readable names to cards
#"Event","CardId","StrokeId","TopX","TopY","BotX","BotY","dx","dy","start","end"
# It is to be noted that topX = left, topY = top, BotX = right and Bot Y= bottom.

names(card.move.s1) <- c("Event","CardId","StrokeId","TopX","TopY","BotX","BotY","dx","dy","start","end","adj.start","adj.end")
names(card.move.s2) <- c("Event","CardId","StrokeId","TopX","TopY","BotX","BotY","dx","dy","start","end","adj.start","adj.end")

# Convert All Values to Numeric for student 1
card.move.s1$CardId <- as.numeric(card.move.s1$CardId)
card.move.s1$TopX <- as.numeric(card.move.s1$TopX)
card.move.s1$TopY <- as.numeric(card.move.s1$TopY)
card.move.s1$BotX <- as.numeric(card.move.s1$BotX)
card.move.s1$BotY <- as.numeric(card.move.s1$BotY)
  
# Convert All Values to Numeric for student 2
card.move.s2$CardId <- as.numeric(card.move.s2$CardId)
card.move.s2$TopX <- as.numeric(card.move.s2$TopX)
card.move.s2$TopY <- as.numeric(card.move.s2$TopY)
card.move.s2$BotX <- as.numeric(card.move.s2$BotX)
card.move.s2$BotY <- as.numeric(card.move.s2$BotY)
```

### Extract Solution and Map Cards to solution grid

```r
 # Read Answer Script
  ansScript <- read.table("tableGrid.txt")
  names(ansScript) <- c('LeftX',"RightX","TopY","BotY","Row","Col")
  ansScript$TopY <- as.numeric(as.character(ansScript$TopY))
  ansScript$BotY <- as.numeric(as.character(ansScript$BotY))
  # Read Answer Details
  IdDesc <- read.table("IdDesc.txt")
  names(IdDesc)<- c("Id","Desc")
  
  #Calculate the MidPoint of each card if available
  card.move.s1$cardX <- (card.move.s1$TopX+card.move.s1$BotX)/2
  card.move.s1$cardY <- (card.move.s1$TopY+card.move.s1$BotY)/2
  
  card.move.s2$cardX <- (card.move.s2$TopX+card.move.s2$BotX)/2
  card.move.s2$cardY <- (card.move.s2$TopY+card.move.s2$BotY)/2
  
  
  
  # Initiate all rows and cols to -1
  card.move.s1$row = -1
  card.move.s2$row = -1
  card.move.s1$col = -1
  card.move.s1$col = -1
  
  # Get All Matches for Student 1
  row.col.match.s1<- apply(card.move.s1,1,function(x) assignCurrentRow(x,ansScript))
  row.col.s1 <- data.frame(matrix(unlist(row.col.match.s1), nrow=length(row.col.match.s1),byrow =T))
  card.move.s1[,c("row","col")] <- row.col.s1
  
  # Get All Matches for Student 2
  row.col.match.s2<- apply(card.move.s2,1,function(x) assignCurrentRow(x,ansScript))
  row.col.s2 <- data.frame(matrix(unlist(row.col.match.s2), nrow=length(row.col.match.s2),byrow =T))
  card.move.s2[,c("row","col")] <- row.col.s2
  
  card.move.s2$Student <- "S2"
  card.move.s1$Student <- "S1"
```

# Get overlaps between two student tiers


```r
getOverlaps <-function(segm.point,segm.all){
  ac1 <- as.numeric(segm.point["start"])
  ac2 <- as.numeric(segm.point["end"])
  
  startBetween <- ( segm.all$start >= ac1  &  segm.all$start<= ac2)
  endBetween <- ( segm.all$end >= ac1  &  segm.all$end<= ac2)
  choose <- (startBetween | endBetween)
  all.id.matches<- segm.all[choose,"id"]
  if(!length(all.id.matches)== 0){
    all.id.matches <- append(all.id.matches,segm.point["id"])
    print("CHECK THIS for intersection")
  }
  all.id.matches
}
```



#### Create Card Segments 

```r
 # Remove Card 1 from the card set
  card.move.s1<- card.move.s1[!card.move.s1$CardId==1,]
  card.move.s2<- card.move.s2[!card.move.s2$CardId==1,]
  
  clean.m.s1<- card.move.s1[!card.move.s1$row==-1,]
  clean.m.s2<- card.move.s2[!card.move.s2$row==-1,]
  
  # Removal of small jitters inside the Grid / Remove repeats. We Pick the Last one!
  clean.m.s1 <- clean.m.s1[cumsum(rle(clean.m.s1$CardId)$lengths),]
  clean.m.s2 <- clean.m.s2[cumsum(rle(clean.m.s2$CardId)$lengths),]
  
  #Create Unique Row col matrix based on CardID and row and column
  clean.m.s1$marker <- paste(clean.m.s1$CardId,"|",clean.m.s1$row,"|",clean.m.s1$col,sep="")
  clean.m.s2$marker <- paste(clean.m.s2$CardId,"|",clean.m.s2$row,"|",clean.m.s2$col,sep="")
  
  # Get indexes of First occurence of Cards in specific row and specific column
  m.s1.ind <- match(unique(clean.m.s1$marker),clean.m.s1$marker)
  m.s2.ind <- match(unique(clean.m.s2$marker),clean.m.s2$marker)
  
  # Get the first occurence of cards in specific rows and columns
  segm.move.s1 <-  clean.m.s1[m.s1.ind,]
  segm.move.s2 <- clean.m.s2[m.s2.ind,]
  
  # Add ids to segments
  segm.move.s1$id <- paste("S1",seq(1:nrow(segm.move.s1)),sep=".")
  segm.move.s2$id <-paste("S2",seq(1:nrow(segm.move.s2)),sep=".")
```

### Get Overlaps between two tiers. The calculation of these are pending



```r
  all.overlaps<- apply(segm.move.s1,1,function(x) getOverlaps(x,segm.move.s2))
  all.overlaps <- unique(as.vector(all.overlaps))

  if(length(all.overlaps) > 0){
  print("CHECK FOR OVERLAP PLEASE!!")
  S1.overlaps <- all.overlaps[grepl("S1.",all.overlaps)]
  S2.overlaps <- all.overlaps[grepl("S2.",all.overlaps)]
  
  segm.move.s1$overlap<-ifelse(segm.move.s1$id %in% S1.overlaps,"O","N")
  segm.move.s2$overlap<-ifelse(segm.move.s2$id %in% S2.overlaps,"O","N")
  
  }
```


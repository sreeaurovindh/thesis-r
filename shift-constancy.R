
getConstancyShift <- function(allData,sessionId){
session.1 <- allData[allData$session == sessionId,]
labels<- session.1[,c("Label")]
prev.labels <- append(0,labels)
prev.labels <- prev.labels[1:(length(prev.labels)-1)]
session.1 <- cbind(session.1,prev.labels)
session.1 <- cbind(session.1,as.numeric(labels))
names(session.1) <- c("session","Label","prev.label","label.num")
session.1$constancyFactor<- as.numeric(session.1$prev.label == session.1$label.num)
session.1<-cbind(session.1,append(0,abs(diff(session.1$label.num))))
names(session.1) <- c("session","Label","prev.label","label.num","constancyFactor","shiftFactor")
constancyIndex <- sum(session.1$constancyFactor) / (nrow(session.1) -1)
shiftIndex <- sum(session.1$shiftFactor)/ (3 * (nrow(session.1) -1))
values <- append(sessionId,constancyIndex)
values <- append(values,shiftIndex)
values
}
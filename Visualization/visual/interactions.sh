set -x
trap read debug
bedtools intersect -a t1.bed -b t2.bed > t1_i_t2.bed
bedtools intersect -a w1.bed -b w2.bed > w1_i_w2.bed
bedtools intersect -a w1.bed -b t1.bed > w1_i_t1.bed
bedtools intersect -a w2.bed -b t1.bed > w2_i_t1.bed
bedtools intersect -a w1.bed -b t2.bed > w1_i_t2.bed
bedtools intersect -a w2.bed -b t2.bed > w2_i_t2.bed



bedtools intersect -a w1_i_w2.bed -b t1.bed > w1_i_w2_i_t1.bed
bedtools intersect -a w1_i_w2.bed -b t2.bed > w1_i_w2_i_t2.bed
bedtools intersect -a t1_i_t2.bed -b w1.bed > t1_i_t2_i_w1.bed
bedtools intersect -a t1_i_t2.bed -b w2.bed > t1_i_t2_i_w2.bed

# a= t1_i_t2 intersect w1_i_w2  **
bedtools intersect -a t1_i_t2.bed -b w1_i_w2.bed > a.bed

if [ -s a.bed ] ; then
# b = w1_i_w2_t1 - a
bedtools subtract -a w1_i_w2_i_t1.bed -b a.bed > b.bed

#c = w1_i_w2_i_t2 - a
bedtools subtract -a w1_i_w2_i_t2.bed -b a.bed > c.bed

#d = t1_i_t2_i_w1 - a
bedtools subtract -a t1_i_t2_i_w1.bed -b a.bed > d.bed

#e = t1_i_t2_i_w2 - a
bedtools subtract -a t1_i_t2_i_w2.bed -b a.bed > e.bed

else
cp w1_i_w2_i_t1.bed b.bed
cp w1_i_w2_i_t2.bed c.bed
cp t1_i_t2_i_w1.bed d.bed
cp t1_i_t2_i_w2.bed e.bed
fi ;


 
#f = w1_i_w2 - b -c -d -e 
if [ -s w1_i_w2.bed ]
 then

	if [ -s b.bed ] 
	 then
		bedtools subtract -a w1_i_w2.bed -b b.bed > temp_1.bed

	else
		cp w1_i_w2.bed temp_1.bed
	fi;

	if  [ -s c.bed ] 
	 then
		bedtools subtract -a temp_1.bed -b c.bed > temp_2.bed
	else
		cp temp_1.bed temp_2.bed
	fi;

	if  [ -s d.bed ] 
	then
		bedtools subtract -a temp_2.bed -b d.bed > temp_3.bed
	else
		cp temp_2.bed temp_3.bed
	fi;

	if  [ -s e.bed ] 
	 then
		bedtools subtract -a temp_3.bed -b e.bed > f.bed
	else
		cp temp_3.bed f.bed
	fi;

else
cp w1_i_w2.bed f.bed
fi;

#g = w1_i_t1 -b -c -d -e
if [ -s w1_i_t1.bed ] 
 then

	if [ -s b.bed ] 
	 then
		bedtools subtract -a w1_i_t1.bed -b b.bed > temp_4.bed

	else
		cp w1_i_t1.bed temp_4.bed
	fi;

	if  [ -s c.bed ] 
	 then
		bedtools subtract -a temp_4.bed -b c.bed > temp_5.bed
	else
		cp temp_4.bed temp_5.bed
	fi;

	if  [ -s d.bed ] 
	then
		bedtools subtract -a temp_5.bed -b d.bed > temp_6.bed
	else
		cp temp_5.bed temp_6.bed
	fi;

	if  [ -s e.bed ]
	then
		bedtools subtract -a temp_6.bed -b e.bed > g.bed
	else
		cp temp_6.bed g.bed
	fi;

else
cp w1_i_t1.bed g.bed
fi;

 
# #h = w2_i_t1 - b -c -d -e 

if [ -s w2_i_t1.bed ] 
 then

	if [ -s b.bed ] 
	 then
		bedtools subtract -a w2_i_t1.bed -b b.bed > temp_7.bed

	else
		cp w2_i_t1.bed temp_7.bed
	fi;

	if  [ -s c.bed ] 
	 then
		bedtools subtract -a temp_7.bed -b c.bed > temp_8.bed
	else
		cp temp_7.bed temp_8.bed
	fi;

	if  [ -s d.bed ] 
	 then
		bedtools subtract -a temp_8.bed -b d.bed > temp_9.bed
	else
		cp temp_8.bed temp_9.bed
	fi;

	if  [ -s e.bed ] 
	then
		bedtools subtract -a temp_9.bed -b e.bed > h.bed
	else
		cp temp_9.bed h.bed
	fi;

else
cp w2_i_t1.bed h.bed
fi;

 


# #i = w1_i_t2 -b -c -d -e 

if [ -s w1_i_t2.bed ] 
	then

	if [ -s b.bed ] 
	 then
		bedtools subtract -a w1_i_t2.bed -b b.bed > temp_10.bed

	else
		cp w1_i_t2.bed temp_10.bed
	fi;

	if  [ -s c.bed ] 
	 then
		bedtools subtract -a temp_10.bed -b c.bed > temp_11.bed
	else
		cp temp_10.bed temp_11.bed
	fi;

	if  [ -s d.bed ] 
	 then
		bedtools subtract -a temp_11.bed -b d.bed > temp_12.bed
	else
		cp temp_11.bed temp_12.bed
	fi;

	if  [ -s e.bed ] 
	then
		bedtools subtract -a temp_12.bed -b e.bed > i.bed
	else
		cp temp_12.bed i.bed
	fi;

else
cp w1_i_t2.bed i.bed
fi;

 

# #j = w2_i_t2 - b -c -d -e
if [ -s w2_i_t2.bed ] 
	then

	if [ -s b.bed ] 
	 then
		bedtools subtract -a w2_i_t2.bed -b b.bed > temp_13.bed

	else
		cp w2_i_t2.bed temp_13.bed
	fi;

	if  [ -s c.bed ] 
	then
		bedtools subtract -a temp_13.bed -b c.bed > temp_14.bed
	else
		cp temp_13.bed temp_14.bed
	fi;

	if  [ -s d.bed ] 
	then
		bedtools subtract -a temp_14.bed -b d.bed > temp_15.bed
	else
		cp temp_14.bed temp_15.bed
	fi;

	if  [ -s e.bed ] 
    then
		bedtools subtract -a temp_15.bed -b e.bed > j.bed
	else
		cp temp_15.bed j.bed
	fi;

else
cp w2_i_t2.bed j.bed
fi;
 

# #k = t1_i_t2 -b -c -d -e

if [ -s t1_i_t2.bed ] 
 then

	if [ -s b.bed ] 
	 then
		bedtools subtract -a t1_i_t2.bed -b b.bed > temp_16.bed

	else
		cp t1_i_t2.bed temp_16.bed
	fi;

	if  [ -s c.bed ] 
	 then
		bedtools subtract -a temp_16.bed -b c.bed > temp_17.bed
	else
		cp temp_16.bed temp_17.bed
	fi;

	if  [ -s d.bed ] 
	 then
		bedtools subtract -a temp_17.bed -b d.bed > temp_18.bed
	else
		cp temp_17.bed temp_18.bed
	fi;

	if  [ -s e.bed ] 
	 then
		bedtools subtract -a temp_18.bed -b e.bed > k.bed
	else
		cp temp_18.bed k.bed
	fi;

else
cp t1_i_t2.bed k.bed
fi;

 #l = w1 - f -g -h -i -j -k -b -c -d -e
if [ -s w1.bed ] 
 then
 	if [ -s f.bed ] 
	 then
		bedtools subtract -a w1.bed -b f.bed > temp_19.bed
	else
		cp w1.bed temp_19.bed
	fi;
	if [ -s g.bed ] 
	 then
		bedtools subtract -a temp_19.bed -b g.bed > temp_20.bed
	else
		cp temp_19.bed temp_20.bed
	fi;
	if [ -s h.bed ] 
	 then
		bedtools subtract -a temp_20.bed -b h.bed > temp_21.bed
	else
		cp temp_20.bed temp_21.bed
	fi;
	if [ -s i.bed ] 
	 then
		bedtools subtract -a temp_21.bed -b i.bed > temp_22.bed
	else
		cp temp_21.bed temp_22.bed
	fi;
	if [ -s j.bed ] 
	 then
		bedtools subtract -a temp_22.bed -b j.bed > temp_23.bed
	else
		cp temp_22.bed temp_23.bed
	fi;
	if [ -s k.bed ] 
	 then
		bedtools subtract -a temp_23.bed -b k.bed > temp_53.bed
	else
		cp temp_23.bed temp_53.bed
	fi;
	if [ -s b.bed ] 
	 then
		bedtools subtract -a temp_53.bed -b b.bed > temp_54.bed
	else
		cp temp_53.bed temp_54.bed
	fi;
	if [ -s c.bed ] 
	 then
		bedtools subtract -a temp_54.bed -b c.bed > temp_55.bed
	else
		cp temp_54.bed temp_55.bed
	fi;
	if [ -s d.bed ] 
	 then
		bedtools subtract -a temp_55.bed -b d.bed > temp_56.bed
	else
		cp temp_55.bed temp_56.bed
	fi;
	if [ -s e.bed ] 
	 then
		bedtools subtract -a temp_56.bed -b e.bed > l.bed
	else
		cp temp_56.bed l.bed
	fi;
	

else
	cp w1.bed l.bed
fi;

#m = w2  - f -g -h -i -j -k 
if [ -s w2.bed ] 
 then
 	if [ -s f.bed ] 
	 then
		bedtools subtract -a w2.bed -b f.bed > temp_24.bed
	else
		cp w2.bed temp_24.bed
	fi;
	if [ -s g.bed ] 
	 then
		bedtools subtract -a temp_24.bed -b g.bed > temp_25.bed
	else
		cp temp_24.bed temp_25.bed
	fi;
	if [ -s h.bed ] 
	 then
		bedtools subtract -a temp_25.bed -b h.bed > temp_26.bed
	else
		cp temp_25.bed temp_26.bed
	fi;
	if [ -s i.bed ] 
	 then
		bedtools subtract -a temp_26.bed -b i.bed > temp_27.bed
	else
		cp temp_26.bed temp_27.bed
	fi;
	if [ -s j.bed ] 
	 then
		bedtools subtract -a temp_27.bed -b j.bed > temp_28.bed
	else
		cp temp_27.bed temp_28.bed
	fi;
	if [ -s k.bed ] 
	 then
		bedtools subtract -a temp_28.bed -b k.bed > temp_57.bed
	else
		cp temp_28.bed temp_57.bed
	fi;
	if [ -s b.bed ] 
	 then
		bedtools subtract -a temp_57.bed -b b.bed > temp_58.bed
	else
		cp temp_57.bed temp_58.bed
	fi;
	if [ -s c.bed ] 
	 then
		bedtools subtract -a temp_58.bed -b c.bed > temp_59.bed
	else
		cp temp_58.bed temp_59.bed
	fi;
	if [ -s d.bed ] 
	 then
		bedtools subtract -a temp_59.bed -b d.bed > temp_60.bed
	else
		cp temp_59.bed temp_60.bed
	fi;
	if [ -s e.bed ] 
	 then
		bedtools subtract -a temp_60.bed -b e.bed > m.bed
	else
		cp temp_60.bed m.bed
	fi;

	

else
	cp w2.bed m.bed
fi;

#n = t1 - - f -g -h -i -j -k 

if [ -s t1.bed ] 
 then
 	if [ -s f.bed ] 
	 then
		bedtools subtract -a t1.bed -b f.bed > temp_29.bed
	else
		cp t1.bed temp_29.bed
	fi;
	if [ -s g.bed ] 
	 then
		bedtools subtract -a temp_29.bed -b g.bed > temp_30.bed
	else
		cp temp_29.bed temp_30.bed
	fi;
	if [ -s h.bed ] 
	 then
		bedtools subtract -a temp_30.bed -b h.bed > temp_31.bed
	else
		cp temp_30.bed temp_31.bed
	fi;
	if [ -s i.bed ] 
	 then
		bedtools subtract -a temp_31.bed -b i.bed > temp_32.bed
	else
		cp temp_31.bed temp_32.bed
	fi;
	if [ -s j.bed ] 
	 then
		bedtools subtract -a temp_32.bed -b j.bed > temp_33.bed
	else
		cp temp_32.bed temp_33.bed
	fi;
	if [ -s k.bed ] 
	 then
		bedtools subtract -a temp_33.bed -b k.bed > temp_61.bed
	else
		cp temp_33.bed temp_61.bed
	fi;
	if [ -s b.bed ] 
	 then
		bedtools subtract -a temp_61.bed -b b.bed > temp_62.bed
	else
		cp temp_61.bed temp_62.bed
	fi;
	if [ -s c.bed ] 
	 then
		bedtools subtract -a temp_62.bed -b c.bed > temp_63.bed
	else
		cp temp_62.bed temp_63.bed
	fi;
	if [ -s d.bed ] 
	 then
		bedtools subtract -a temp_63.bed -b d.bed > temp_64.bed
	else
		cp temp_63.bed temp_64.bed
	fi;
	if [ -s e.bed ] 
	 then
		bedtools subtract -a temp_64.bed -b e.bed > n.bed
	else
		cp temp_64.bed n.bed
	fi;
	

else
	cp t1.bed n.bed
fi;


#o = t2  - f -g -h -i -j -k  
if [ -s t2.bed ] 
 then
 	if [ -s f.bed ] 
	 then
		bedtools subtract -a t2.bed -b f.bed > temp_34.bed
	else
		cp t2.bed temp_34.bed
	fi;
	if [ -s g.bed ] 
	 then
		bedtools subtract -a temp_34.bed -b g.bed > temp_35.bed
	else
		cp temp_34.bed temp_35.bed
	fi;
	if [ -s h.bed ] 
	 then
		bedtools subtract -a temp_35.bed -b h.bed > temp_36.bed
	else
		cp temp_35.bed temp_36.bed
	fi;
	if [ -s i.bed ] 
	 then
		bedtools subtract -a temp_36.bed -b i.bed > temp_37.bed
	else
		cp temp_36.bed temp_37.bed
	fi;
	if [ -s j.bed ] 
	 then
		bedtools subtract -a temp_37.bed -b j.bed > temp_38.bed
	else
		cp temp_37.bed temp_38.bed
	fi;
	if [ -s k.bed ] 
	 then
		bedtools subtract -a temp_38.bed -b k.bed > temp_65.bed
	else
		cp temp_38.bed temp_65.bed
	fi;
	if [ -s b.bed ] 
	 then
		bedtools subtract -a temp_65.bed -b b.bed > temp_66.bed
	else
		cp temp_65.bed temp_66.bed
	fi;
	if [ -s c.bed ] 
	 then
		bedtools subtract -a temp_66.bed -b c.bed > temp_67.bed
	else
		cp temp_66.bed temp_67.bed
	fi;
	if [ -s d.bed ] 
	 then
		bedtools subtract -a temp_67.bed -b d.bed > temp_68.bed
	else
		cp temp_67.bed temp_68.bed
	fi;
	if [ -s e.bed ] 
	 then
		bedtools subtract -a temp_68.bed -b e.bed > o.bed
	else
		cp temp_68.bed o.bed
	fi;
	

else
	cp t2.bed o.bed
fi;

# p = all - a-b-c-d-e-f-g-h-i-j-k-l-m-n-o

# P will exist because it is the entire session timestamps anyways :)

if [ -s a.bed ] 
then
	bedtools subtract -a all.bed -b a.bed > temp_39.bed
else
	cp all.bed temp_39.bed
fi;

if [ -s b.bed ] 
then
	bedtools subtract -a temp_39.bed -b b.bed > temp_40.bed
else
	cp temp_39.bed temp_40.bed
fi;

if [ -s c.bed ] 
then
	bedtools subtract -a temp_40.bed -b c.bed > temp_41.bed
else
	cp temp_40.bed temp_41.bed
fi;

if [ -s d.bed ] 
then
	bedtools subtract -a temp_41.bed -b d.bed > temp_42.bed
else
	cp temp_41.bed temp_42.bed
fi;

if [ -s e.bed ] 
then
	bedtools subtract -a temp_42.bed -b e.bed > temp_43.bed
else
	cp temp_42.bed temp_43.bed
fi;


if [ -s f.bed ] 
then
	bedtools subtract -a temp_43.bed -b f.bed > temp_44.bed
else
	cp temp_43.bed temp_44.bed
fi;

if [ -s g.bed ] 
then
	bedtools subtract -a temp_44.bed -b g.bed > temp_45.bed
else
	cp temp_44.bed temp_45.bed
fi;

if [ -s h.bed ] 
then
	bedtools subtract -a temp_45.bed -b h.bed > temp_46.bed
else
	cp temp_45.bed temp_46.bed
fi;

if [ -s i.bed ] 
then
	bedtools subtract -a temp_46.bed -b i.bed > temp_47.bed
else
	cp temp_46.bed temp_47.bed
fi;

if [ -s j.bed ] 
then
	bedtools subtract -a temp_47.bed -b j.bed > temp_48.bed
else
	cp temp_47.bed temp_48.bed
fi;

if [ -s k.bed ] 
then
	bedtools subtract -a temp_48.bed -b k.bed > temp_49.bed
else
	cp temp_48.bed temp_49.bed
fi;

if [ -s l.bed ] 
then
	bedtools subtract -a temp_49.bed -b l.bed > temp_50.bed
else
	cp temp_49.bed temp_50.bed
fi;

if [ -s m.bed ] 
then
	bedtools subtract -a temp_50.bed -b m.bed > temp_51.bed
else
	cp temp_50.bed temp_51.bed
fi;

if [ -s n.bed ] 
then
	bedtools subtract -a temp_51.bed -b n.bed > temp_52.bed
else
	cp temp_51.bed temp_52.bed
fi;

if [ -s o.bed ] 
then
	bedtools subtract -a temp_52.bed -b o.bed > p.bed
else
	cp temp_52.bed p.bed
fi;